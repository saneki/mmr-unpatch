#!/usr/bin/env python

import argparse
import collections
import hexdump
import json
import os
import struct
import sys

import csvinfo
import disasm
import dmadata
import patch
import vfile

class InfoWriter(object):
    """ Writes info for `PatchData` entries. """
    def __init__(self, csv=None):
        self._csv = csv

    @property
    def csv(self):
        """ Get the `CsvInfo`. """
        return self._csv

    def print_patch(self, patch, outfile=sys.stdout):
        """ Print patch entry. """
        if self.csv:
            vfile = self.csv.find(patch.addr)
            if vfile is not None:
                offset = patch.addr - vfile.start
                print('VRom File:    {:08X}, VRom Offset:  {:08X} >> {}'.format(vfile.start, offset, vfile.name), file=outfile)
        print('VRom Address: {:08X}, Patch Offset: {:08X}'.format(patch.addr, patch.offset), file=outfile)
        hex = hexdump.hexdump(patch.data, result='return')
        print('{}'.format(hex), file=outfile)
        print(file=outfile)

    def write(self, patches, outfile=sys.stdout):
        """ Write info about each `PatchData` to outfile. """
        for patch in patches:
            self.print_patch(patch, outfile=outfile)

def get_parser():
    """ Get the argument parser. """
    parser = argparse.ArgumentParser(description='MMR Patch Parser')
    parser.add_argument('-c', '--vrom-csv', help='CSV file with VROM file addresses and names')
    parser.add_argument('-m', '--mapping', help='Mapping JSON file')
    parser.add_argument('-r', '--rom-file', help='ROM file')
    parser.add_argument('--disasm', action='store_true', help='Attempt to disassemble entries instead of hexdump')
    parser.add_argument('file', help='Patch file')
    return parser

def main():
    """ Main function. """
    args = get_parser().parse_args()

    curpath = os.path.abspath(os.path.dirname(__file__))
    # Use default mapping file if not specified.
    if args.mapping is None:
        args.mapping = os.path.join(curpath, 'data', 'mapping.json')
    # Use default CSV file if not specified.
    if args.vrom_csv is None:
        args.vrom_csv = os.path.join(curpath, 'data', 'filelist-us-v1.0.csv')

    # Load CsvInfo from CSV file if specified.
    csv = None
    if args.vrom_csv is not None:
        with open(args.vrom_csv, 'r') as csvfile:
            csv = csvinfo.CsvInfo.from_file(csvfile)

    # Ensure --mapping and --rom-file arguments are specified when attempting disassembly.
    if args.disasm and args.mapping is None:
        print('Disassembly option --disasm requires --mapping, aborting.', file=sys.stderr)
        sys.exit(1)
    elif args.disasm and args.rom_file is None:
        print('Disassembly option --disasm requires --rom-file, aborting.', file=sys.stderr)
        sys.exit(1)

    with open(args.file, 'rb') as f:
        entries = patch.read_file(f)
        if args.disasm:
            do_disasm(args, entries, csv)
        else:
            writer = InfoWriter(csv=csv)
            writer.write(entries)

def do_disasm(args, patches, csv):
    with open(args.rom_file, 'rb') as romfile:
        romfile = dmadata.RomFile(romfile, offset=dmadata.DMADATA_MM)
        # Load mapping of VROM/VRAM from JSON file
        with open(args.mapping, 'r') as fp:
            objects = json.load(fp)
            # Initialize resolver and build VFile mapping
            resolver = vfile.VFileResolver(romfile.table, objects, csv=csv, build=True)

        manager = patch.PatchManager(romfile, resolver)
        for patch_data in patches:
            # Disassemble each patch data entry to text
            text = manager.disasm_patch_to_text(patch_data)
            print(text)

if __name__ == '__main__':
    main()
