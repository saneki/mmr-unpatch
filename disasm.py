import capstone
import collections
import io

def get_mips64_cs():
    """ Get a Capstone handle for big-endian MIPS64. """
    return capstone.Cs(capstone.CS_ARCH_MIPS, capstone.CS_MODE_MIPS64 + capstone.CS_MODE_BIG_ENDIAN)

def signed_hex(value, zeros=4, upper=True):
    """ Hacky format for signed hex integer (with sign before `0x` prefix). """
    xc = 'X' if upper is True else 'x'
    fmt = f'0{zeros}{xc}'
    if value < 0:
        return ('-0x{0:' + fmt + '}').format(abs(value))
    else:
        return ('0x{0:' + fmt + '}').format(value)

def try_format_hex_offset_token(token):
    """ Try and format an integer offset in a token as a hex number. """
    subs = token.split('(')
    if len(subs) == 2:
        try:
            value = int(subs[0], 0)
        except ValueError:
            return False, token
        return True, '('.join((signed_hex(value), subs[1]))
    else:
        return False, token

def try_format_hex_token(token):
    """ Try and format a token which is entirely an integer as a hex number. """
    try:
        value = int(token, 0)
    except ValueError:
        return False, token
    return True, signed_hex(value)

def try_fix_token(token):
    """ Try and "fix" a token. """
    funcs = (
        try_format_hex_offset_token,
        try_format_hex_token,
    )
    for func in funcs:
        success, result = func(token)
        if success:
            return result
    return token

class Disasm(object):
    """ Disassembler (wrapper for Capstone). """
    def __init__(self, cs=None):
        if cs is None:
            cs = get_mips64_cs()
        self._cs = cs

    @property
    def cs(self):
        """ Get the Capstone handle. """
        return self._cs

    def disasm(self, instrs, vram):
        """ Disassemble instructions using the Capstone handle. """
        return self.cs.disasm(instrs, vram)

    def disasm_patch_bytes(self, patch_bytes):
        """ Disassemble from `PatchBytes`. """
        def get_instr(dword, addr):
            try:
                return next(self.disasm(dword, addr))
            except StopIteration:
                return None
        return tuple(get_instr(dword, addr) for (dword, _, addr) in patch_bytes.words_bytes())

class DisasmFormatter(object):
    """ Text formatter for disassembly output. """
    def comment(self, line, end='\n'):
        """ Format a comment line. """
        return '; {0}{1}'.format(line, end)

    def dmadata_define(self, vfile):
        """ Format string for defining dmadata file stuff. """
        name = vfile.name.upper()
        vram = vfile.vram.start
        text = DisasmFormatter.get_ram_or_vram(vram)
        dma_line   = 'DefineDmaFile G_{0}_DMA, G_{0}_FILE, {1}'.format(name, vfile.index)
        label_line = '.definelabel  G_{0}_{2}, {1:08X}'.format(name, vram, text)
        return (dma_line, label_line)

    def headersize(self, vfile, direct=True):
        """ Format string for `.headersize` directive. """
        if direct:
            return '.headersize (0x{0:08X} - 0x{1:08X})'.format(vfile.vram.start, vfile.prom.start)
        else:
            name = vfile.name.upper()
            text = DisasmFormatter.get_ram_or_vram(vfile.vram.start)
            return '.headersize (G_{0}_{1} - G_{0}_FILE)'.format(name, text)

    def format_patch_bytes(self, patch_bytes, comment=False):
        """ Format disassembly output for `PatchBytes`. """
        prefix = '    ' if not comment else ';   '
        with io.StringIO() as stream:
            if patch_bytes.is_full_code and patch_bytes.is_aligned and not patch_bytes.has_invalid_instrs:
                for instr in patch_bytes.instrs:
                    line = DisasmFormatter.format_disasm_line(instr)
                    stream.write('{0}{1}\n'.format(prefix, line))
            else:
                for (idx, (word, size, _)) in enumerate(patch_bytes.words()):
                    if not patch_bytes.phys:
                        instr = patch_bytes.instrs[idx]
                    else:
                        instr = None
                    line = DisasmFormatter.format_word_line(word, size, instr=instr)
                    stream.write('{0}{1}\n'.format(prefix, line))
            return stream.getvalue()

    def write_comment_header(self, chunk, stream):
        """ Write comment header to given stream. """
        # Build lines of comment header
        lines = []
        lines.append('File: 0x{0:08X}, Address: 0x{1:08X}, Offset: 0x{2:08X}, Patch: 0x{3:08X}'
            .format(chunk.vfile.vrom.start, chunk.patch.addr, chunk.offset, chunk.patch.offset))
        if chunk.vfile.has_name:
            if chunk.vfile.has_description:
                lines.append('Name: {0} :: {1}'.format(chunk.vfile.name, chunk.vfile.description))
            else:
                lines.append('Name: {0}'.format(chunk.vfile.name))
        # Write lines along with boundary
        boundary = self.comment('=' * len(lines[0]))
        stream.write(boundary)
        for line in lines:
            stream.write(self.comment(line))
        stream.write(boundary)

    def format_patch_chunk(self, chunk):
        """ Format disassembly output for `PatchChunk`. """
        addr = chunk.data.addr
        # Use `.orga` directive if using physical ROM address
        orgdir = 'org' if not chunk.data.phys else 'orga'
        with io.StringIO() as stream:
            self.write_comment_header(chunk, stream)
            if not chunk.data.phys:
                headersize = self.headersize(chunk.vfile)
                stream.write('{0}\n'.format(headersize))
            stream.write(self.comment('Replaces:'))
            stream.write(self.format_patch_bytes(chunk.orig, comment=True))
            stream.write('.{0} 0x{1:08X}\n'.format(orgdir, addr))
            stream.write(self.format_patch_bytes(chunk.data, comment=False))
            return stream.getvalue()

    @staticmethod
    def format_disasm_line(instr):
        """ Format a single instruction as a line of assembly. """
        mnemonic = instr.mnemonic
        op_str = instr.op_str.replace('$zero', '$r0').replace('$', '')
        # De-alias `move` mnemonic to `or` instruction
        if instr.mnemonic == 'move':
            mnemonic = 'or'
            op_str = '{0}, r0'.format(op_str)
        # Try to "fix" individual tokens in op string
        tokens = (try_fix_token(token) for token in op_str.split(', '))
        op_str = ', '.join(tokens)
        return '{0:7} {1}'.format(mnemonic, op_str).rstrip()

    @staticmethod
    def format_word_line(word, size, instr=None):
        """ Format a word as a line of assembly, showing an optional instruction as a comment. """
        if size == 4:
            directive = '.dw 0x{0:08X}'.format(word)
        elif size == 2:
            directive = '.dh 0x{0:04X}'.format(word)
        elif size == 1:
            directive = '.db 0x{0:02X}'.format(word)

        if instr is not None:
            comment = DisasmFormatter.format_disasm_line(instr)
            return '{0} ; {1}'.format(directive, comment)
        else:
            return '{0}'.format(directive)

    @staticmethod
    def get_ram_or_vram(addr):
        """ Get either "RAM" or "VRAM" based on the memory address. """
        if addr < 0x80800000:
            return 'RAM'
        else:
            return 'VRAM'
