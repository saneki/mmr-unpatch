from disasm import Disasm, DisasmFormatter
import struct

class PatchData(object):
    """ Patch data entry. """
    def __init__(self, addr, offset, data):
        self._addr = addr
        self._offset = offset
        self._data = data

    @property
    def addr(self):
        """ Get the virtual ROM address. """
        return self._addr

    @property
    def offset(self):
        """ Get the patch file offset. """
        return self._offset

    @property
    def data(self):
        """ Get the patch data. """
        return self._data

class PatchBytes(object):
    """ Patch bytes. """
    def __init__(self, data, addr, disasm=None, phys=False):
        self._addr = addr
        self._data = data
        self._phys = phys
        if disasm is not None:
            self._instrs = self.parse(disasm)

    @property
    def addr(self):
        """ Address of bytes in memory. """
        return self._addr

    @property
    def data(self):
        """ Get the raw bytes. """
        return self._data

    @property
    def has_invalid_instrs(self):
        """ Check whether or not any parsed instructions are "invalid". """
        # MOVF instruction is too recent
        invalid = ['movf']
        for instr in self.instrs:
            if instr is not None and instr.mnemonic in invalid:
                return True
        return False

    @property
    def instrs(self):
        """ Get all parsed instructions. """
        return self._instrs

    @property
    def is_aligned(self):
        """ Get whether or not the address is 4-byte aligned. """
        return self.addr % 4 == 0

    @property
    def is_full_code(self):
        """ Get whether or not all bytes were successfully parsed into instructions. """
        return not self.phys and not (None in self.instrs)

    @property
    def phys(self):
        """ Get whether or not the address is a physical ROM address. Used when a VRAM address is not available. """
        return self._phys

    def words_bytes(self):
        """ Get data as words (in `bytes` form). """
        for offset in range(0, len(self.data), 4):
            window = self.data[offset:offset+4]
            if len(window) == 4:
                yield (self.data[offset:offset+4], 4, self.addr + offset)
            elif len(window) == 3:
                yield (self.data[offset:offset+2], 2, self.addr + offset)
                offset += 2
                yield (self.data[offset:offset+1], 1, self.addr + offset)
            elif len(window) == 2:
                yield (self.data[offset:offset+2], 2, self.addr + offset)
            elif len(window) == 1:
                yield (self.data[offset:offset+1], 1, self.addr + offset)

    def words(self):
        """ Get data as words. """
        for (word, size, addr) in self.words_bytes():
            if size == 4:
                value = struct.unpack('>I', word)[0]
            elif size == 2:
                value = struct.unpack('>H', word)[0]
            elif size == 1:
                value = struct.unpack('B', word)[0]
            yield (value, size, addr)

    def parse(self, disasm):
        """ Disassemble patch bytes to instructions. """
        return disasm.disasm_patch_bytes(self)

class PatchChunk(object):
    """ Patch "chunk" with `PatchBytes` (for new and original data), with relevant `VFile`. """
    def __init__(self, patch, original_bytes, vfile, disasm=None):
        self._patch = patch
        self._vfile = vfile
        self._bytes = (
            self._create_patch_bytes(patch.data, disasm=disasm),
            self._create_patch_bytes(original_bytes, disasm=disasm)
        )

    def _create_patch_bytes(self, data, disasm=None):
        """ Create a `PatchBytes`. """
        vrom = self.patch.addr
        if self.vfile.mapping is not None:
            # If VRAM accessible, use it (for using `.org` directive)
            vram = self.vfile.resolve_vram(vrom)
            return PatchBytes(data, vram, disasm=disasm)
        else:
            # If no VRAM accessible, fallback to physical ROM address (for using `.orga` directive)
            prom = self.vfile.resolve_prom(vrom)
            return PatchBytes(data, prom, phys=True)

    @property
    def data(self):
        """ Get the `PatchBytes` of the new patch data. """
        return self._bytes[0]

    @property
    def offset(self):
        """ Get the offset into the `VFile`. """
        return self.patch.addr - self.vfile.vrom.start

    @property
    def orig(self):
        """ Get the `PatchBytes` of the original patch data. """
        return self._bytes[1]

    @property
    def patch(self):
        """ Get the `PatchData` object. """
        return self._patch

    @property
    def vfile(self):
        """ Get the `VFile` object. """
        return self._vfile

class PatchManager(object):
    """ Patch manager. """
    def __init__(self, romfile, resolver):
        self._disasm = Disasm()
        self._formatter = DisasmFormatter()
        self._resolver = resolver
        self._romfile = romfile

    @property
    def disasm(self):
        """ Get the `Disasm` for disassembling. """
        return self._disasm

    @property
    def formatter(self):
        """ Get the `DisasmFormatter` for formatting disassembly output. """
        return self._formatter

    @property
    def resolver(self):
        """ Get the `VFileResolver`. """
        return self._resolver

    @property
    def romfile(self):
        """ Get the `RomFile`. """
        return self._romfile

    def get_chunk(self, patch):
        """ Create a `PatchChunk` object for a specific `PatchData`. """
        vfile = self.resolver.resolve(patch.addr)
        origdata = self.romfile.read(patch.addr, len(patch.data))
        return PatchChunk(patch, origdata, vfile, disasm=self.disasm)

    def disasm_patch_to_text(self, patch):
        """ Disassemble a `PatchData` entry to text. """
        chunk = self.get_chunk(patch)
        return self.formatter.format_patch_chunk(chunk)

def read_file(f):
    """ Read all `PatchData` entries in a given file. """
    patches = []

    while True:
        pos = f.tell()
        if f.read(1) == b'\xFF':
            return tuple(patches)
        f.seek(pos)

        header = f.read(8)
        if len(header) < 8:
            raise Exception('Incomplete patch header at entry: 0x{:08X}'.format(pos))
        (address, length) = struct.unpack('>II', header)
        data = f.read(length)
        if len(data) != length:
            raise Exception('Incomplete patch data at entry: 0x{:08X}'.format(pos))
        patches.append(PatchData(address, pos, data))

    return tuple(patches)
