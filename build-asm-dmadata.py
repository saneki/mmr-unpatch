import argparse
import json
import sys
from types import SimpleNamespace
from typing import Tuple

import csvinfo
import dmadata
import util

def get_macro_lines(entry, row: csvinfo.CsvRow, data: dmadata.TableEntry) -> Tuple[str, str, str, str]:
    """ Build armips macro lines for a specific virtual file. """
    name = entry.name.upper()
    vram_text = 'RAM' if (entry.vram_start < 0x80800000) else 'VRAM'
    desc = ' :: {0}'.format(row.description) if row.has_description else ''
    return (
        '; Overlay: {0}{1}'.format(entry.name, desc),
        '.definelabel G_{0}_FILE, 0x{1:X}'.format(name, data.phys_start),
        '.definelabel G_{0}_{1}, 0x{2:08X}'.format(name, vram_text, entry.vram_start),
        '.definelabel G_{0}_DELTA, (G_{0}_{1} - G_{0}_FILE)'.format(name, vram_text),
    )

def get_parser():
    """ Get the argument parser. """
    parser = argparse.ArgumentParser(description="Dump armips macros for dmadata table.")
    parser.add_argument('mapping', help='JSON mapping file')
    parser.add_argument('filelist', help='CSV filelist file')
    parser.add_argument('romfile', help='Decompressed ROM file')
    return parser

def write_macro_lines(mapping, filelist: csvinfo.CsvInfo, table: dmadata.Table, outfile=sys.stdout):
    """ Build and write armips macro lines for the given mapping. """
    rows = iter(filelist.rows)
    table_iter = iter(table.entries)
    for idx, obj in enumerate(mapping):
        entry = SimpleNamespace(**obj)
        row = util.takeuntil(lambda x: x.start == entry.vrom_start, rows)
        data = util.takeuntil(lambda x: x.virt_start == entry.vrom_start, table_iter)
        lines = get_macro_lines(entry, row, data)
        text = '\n'.join(lines)
        if idx != 0:
            text = '\n{0}\n'.format(text)
        else:
            text = '{0}\n'.format(text)
        outfile.write(text)

def main():
    """ Main function. """
    args = get_parser().parse_args()
    with open(args.mapping, 'r') as f:
        mapping = json.load(f)
    filelist = csvinfo.CsvInfo.from_path(args.filelist)
    table = dmadata.Table.read_path(args.romfile, dmadata.DMADATA_MM)
    write_macro_lines(mapping, filelist, table)

if __name__ == '__main__':
    main()
