#!/usr/bin/env python

import argparse
import collections
import ctypes
import itertools
import json
import operator
import sys

import csvinfo
import util

VRAMMapping = collections.namedtuple('VRAMMapping', ('vrom_start', 'vrom_end', 'vram_start', 'vram_end'))

CODE_MAPPING = VRAMMapping(vrom_start=0x00B3C000, vrom_end=0x00C7A4E0, vram_start=0x800A5AC0, vram_end=0x801E3FA0)

class ArrayDeserializer(object):
    """ Deserializer for arrays of structures in a file. """
    def __init__(self, kls, start, end):
        self._kls = kls
        self._start = start
        self._end = end

    @property
    def kls(self):
        """ Get ctypes type. """
        return self._kls

    @property
    def start(self):
        """ Get file start offset. """
        return self._start

    @property
    def end(self):
        """ Get file end offset. """
        return self._end

    @property
    def size(self):
        """ Get size of full chunk. """
        return self.end - self.start

    @property
    def itemsize(self):
        """ Get size of an individual array item. """
        return ctypes.sizeof(self.kls())

    def generate(self, fp):
        """ Read and generate all entries from a file pointer. """
        fp.seek(self.start)
        while fp.tell() < self.end:
            data = fp.read(self.itemsize)
            inst, success = util.deserialize(self.kls, data)
            if not success:
                break
            yield inst

class OverlayStructure(ctypes.BigEndianStructure):
    """ Base type for overlay structures. """
    @property
    def mapping(self):
        """ Get VRAMMapping. """
        return VRAMMapping(self.vrom_start, self.vrom_end, self.vram_start, self.vram_end)

class ActorOvl(OverlayStructure):
    """ Actor overlay structure. """
    _fields_ = (
        ('vrom_start', ctypes.c_uint32),
        ('vrom_end',   ctypes.c_uint32),
        ('vram_start', ctypes.c_uint32),
        ('vram_end',   ctypes.c_uint32),
        ('p_ram',      ctypes.c_uint32),
        ('p_init',     ctypes.c_uint32),
        ('p_filename', ctypes.c_uint32),
        ('alloc_type', ctypes.c_uint16),
        ('loaded',     ctypes.c_uint8),
        ('padding',    ctypes.c_uint8),
    )

class EffectOvl(OverlayStructure):
    """ Actor overlay structure for effect actors. """
    _fields_ = (
        ('vrom_start', ctypes.c_uint32),
        ('vrom_end',   ctypes.c_uint32),
        ('vram_start', ctypes.c_uint32),
        ('vram_end',   ctypes.c_uint32),
        ('p_ram',      ctypes.c_uint32),
        ('p_init',     ctypes.c_uint32),
        ('flags',      ctypes.c_uint32),
    )

class GameStateOvl(OverlayStructure):
    """ GameState overlay structure. """
    _fields_ = (
        ('p_ram',      ctypes.c_uint32),
        ('vrom_start', ctypes.c_uint32),
        ('vrom_end',   ctypes.c_uint32),
        ('vram_start', ctypes.c_uint32),
        ('vram_end',   ctypes.c_uint32),
        ('unknown14',  ctypes.c_uint32),
        ('p_init',     ctypes.c_uint32),
        ('p_destroy',  ctypes.c_uint32),
        ('unknown20',  ctypes.c_uint32),
        ('unknown24',  ctypes.c_uint32),
        ('unknown28',  ctypes.c_uint32),
        ('inst_size',  ctypes.c_uint32),
    )

class PlayerOvl(OverlayStructure):
    """ Player overlay structure. """
    _fields_ = (
        ('p_ram',      ctypes.c_uint32),
        ('vrom_start', ctypes.c_uint32),
        ('vrom_end',   ctypes.c_uint32),
        ('vram_start', ctypes.c_uint32),
        ('vram_end',   ctypes.c_uint32),
        ('unknown',    ctypes.c_uint32),
        ('p_filename', ctypes.c_uint32),
    )

ACTOR_DES  = ArrayDeserializer(ActorOvl, 0x1AEFF0, 0x1B4610)
EFFECT_DES = ArrayDeserializer(EffectOvl, 0x1AE4A0, 0x1AE8E4)
GAMEST_DES = ArrayDeserializer(GameStateOvl, 0x1BD910, 0x1BDA60)
PLAYER_DES = ArrayDeserializer(PlayerOvl, 0x1D0B70, 0x1D0BA8)

def valid(mapping):
    """ Get whether or not a mapping appears valid. """
    return not (mapping.vrom_start == mapping.vrom_end == 0)

def read(fp):
    """ Read tables from a RAM dump file and return mappings. """
    def mapping(generator):
        """ Get valid mappings from a generator. """
        return tuple(x.mapping for x in generator if valid(x.mapping))
    actors = ACTOR_DES.generate(fp)
    effects = EFFECT_DES.generate(fp)
    gamestate = GAMEST_DES.generate(fp)
    players = PLAYER_DES.generate(fp)
    return {
        'code': (CODE_MAPPING,),
        'actors': mapping(actors),
        'effects': mapping(effects),
        'gamestate': mapping(gamestate),
        'players': mapping(players),
    }

def print_section(name, items, outfile=sys.stdout):
    """ Print a section of mappings. """
    print('{}:'.format(name), file=outfile)
    for item in items:
        print(' {:08X} => {:08X}'.format(item.vrom_start, item.vram_start), file=outfile)

def dump_json(results, csv_info=None, indent=2, outfile=sys.stdout):
    """ Dump as JSON to a file. """
    # Take all values (tuples of VRAMMapping) and concat into one, then sort
    values = (v for (k, v) in results.items())
    values = itertools.chain(*values)
    values = (x._asdict() for x in values)
    array = sorted(values, key=lambda x: x['vrom_start'])

    # Update with names from CSV file, very inefficient for now
    if csv_info is not None:
        for row in csv_info.rows:
            # Ignore files with unknown names
            if row.name == '(?)':
                continue
            for item in array:
                if item['vrom_start'] == row.start:
                    item['name'] = row.name

    json.dump(array, outfile, indent=indent)
    outfile.write('\n')

def get_parser():
    """ Get the argument parser. """
    parser = argparse.ArgumentParser(description="Dump RAM tables from Majora's Mask (VROM -> VRAM)")
    parser.add_argument('-c', '--vrom-csv', help='CSV file used to retrieve filenames')
    parser.add_argument('-j', '--to-json', action='store_true', help='Output as JSON')
    parser.add_argument('file', help='RAM dump file')
    return parser

def main():
    """ Main function. """
    args = get_parser().parse_args()

    # Parse the CSV file if any
    csv_path = args.vrom_csv
    csv_info = csvinfo.CsvInfo.from_path(csv_path) if (csv_path is not None) else None

    # Read and parse the RAM dump file and output information
    with open(args.file, 'rb') as fp:
        results = read(fp)
        if args.to_json:
            dump_json(results, csv_info=csv_info)
        else:
            for name, items in results.items():
                print_section(name, items)

if __name__ == '__main__':
    main()
