#!/usr/bin/env python

import argparse
import ctypes

import util

class GetItemEntry(ctypes.BigEndianStructure):
    """ Entry for the "get-item" table. """
    _fields_ = (
        ('item', ctypes.c_ubyte),
        ('flag', ctypes.c_ubyte),
        ('graphic', ctypes.c_ubyte),
        ('type', ctypes.c_ubyte),
        ('message', ctypes.c_ushort),
        ('object', ctypes.c_ushort),
    )

    @property
    def string(self):
        """ Get an alternate string representation. """
        return '{{ Item=0x{0:02X}, Flag=0x{1:02X}, Gfx=0x{2:02X}, Type=0x{3:02X}, Msg=0x{4:04X}, Obj=0x{5:04X} }}'.format(
            self.item, self.flag, self.graphic, self.type, self.message, self.object)

    @staticmethod
    def size():
        """ Get the structure size. """
        return ctypes.sizeof(GetItemEntry())

def get_parser():
    def auto_int(x):
        """ Parse int. """
        return int(x, 0)
    parser = argparse.ArgumentParser(description='Parser for gi-table mod file')
    parser.add_argument('-i', '--index', type=auto_int, default=None, help='Index to select')
    parser.add_argument('file', help='gi-table file')
    return parser

def parse_each(fp):
    """ Parse each `GetItemEntry` from a file. """
    size = GetItemEntry.size()
    while True:
        position = fp.tell()
        data = fp.read(size)
        if len(data) < size:
            break
        entry, success = util.deserialize(GetItemEntry, data)
        if success:
            yield entry
        else:
            raise Exception('Error reading GetItemEntry at offset: 0x{0:08X}'.format(position))

def parse_all(fp):
    """ Parse all `GetItemEntry` entries into a tuple. """
    return tuple(parse_each(fp))

def show(entries, index=None):
    """ Show all entries, or one of a specific index. """
    for idx, entry in enumerate(entries):
        if index is None or index == idx:
            print('[0x{0:04X}] = {1}'.format(idx, entry.string))

def main():
    """ Main function. """
    args = get_parser().parse_args()
    with open(args.file, 'rb') as fp:
        entries = parse_all(fp)
        show(entries, index=args.index)

if __name__ == '__main__':
    main()
