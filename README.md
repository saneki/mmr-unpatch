mmr-unpatch
===========

Experimental code for auto-disassembling mod files for [Majora's Mask Randomizer][MMR].

### Dependencies

A few packages are required which can be installed via `pip`:

```sh
python -m pip install capstone hexdump sortedcollections
```

### Usage Examples

Example snippet for disassembling `mods/replace-gi-table`:

```sh
python mmrpatch.py --disasm -r MM_Decompressed.z64 mods/replace-gi-table
```

[MMR]:https://github.com/ZoeyZolotova/mm-rando
