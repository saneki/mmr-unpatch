import collections
from sortedcontainers import SortedDict

import util

MappingObj = collections.namedtuple('MappingObj', ('vrom_start', 'vrom_end', 'vram_start', 'vram_end', 'name'))

class VFile(object):
    """ Virtual file information. """
    def __init__(self, table_entry, mapping_obj=None, csv_row=None):
        self._table_entry = table_entry
        self._mapping_obj = mapping_obj
        self._csv_row = csv_row

    @property
    def description(self):
        """ Get the file description from the CSV row data. """
        if self.row is not None:
            return self.row.description

    @property
    def entry(self):
        """ Get the `TableEntry` which was retrieved from the ROM file. """
        return self._table_entry

    @property
    def has_description(self):
        """ Get whether or not this `VFile` has a known description. """
        return self.row is not None and self.row.has_description

    @property
    def has_name(self):
        """ Get whether or not this `VFile` has a known name. """
        return self.row is not None and self.row.has_name

    @property
    def index(self):
        """ Get the file index. """
        return self.entry.index

    @property
    def mapping(self):
        """ Get "mapping" data used to store VRAM addresses and file name. """
        return self._mapping_obj

    @property
    def name(self):
        """ Get the file name (requires row). """
        if self.row is not None:
            return self.row.name

    @property
    def prom(self):
        """ Get the ROM physical address range. """
        return self.entry.prom

    @property
    def row(self):
        """ Get the CSV row data (if provided). """
        return self._csv_row

    @property
    def vram(self):
        """ Get the VRAM address range (requires mapping). """
        return range(self.mapping.vram_start, self.mapping.vram_end)

    @property
    def vrom(self):
        """ Get the ROM virtual address range. """
        return self.entry.vrom

    def resolve_vram(self, vrom):
        """ Resolve a VRAM address given a virtual ROM address. """
        if vrom in self.vrom:
            delta = vrom - self.vrom.start
            return self.vram.start + delta

    def resolve_prom(self, vrom):
        """ Resolve a physical ROM address given a virtual ROM address. """
        if vrom in self.vrom:
            delta = vrom - self.vrom.start
            return self.prom.start + delta

class VFileResolver(object):
    """ Virtual file resolver. """
    def __init__(self, table, mapping, csv=None, build=False):
        self._table = table
        self._mapping = (MappingObj(**x) for x in mapping)
        self._csv = csv
        self._vfiles = None
        if build:
            self.build()

    def build(self):
        """ Build `VFile` mapping. """
        self._vfiles = self.build_vfiles()

    def build_vfiles(self):
        """ Build the dictionary for mapping virtual ROM addresses to `VFile` instances. """
        vfiles = {}
        # Wrap iterators for mapping objects and CSV rows.
        # This is useful because they are all sorted by virtual ROM address,
        # along with the dmadata table entries. Thus we can iterate them all
        # together without iterating any more than once.
        mapiter = util.IterWrap(iter(self.mapping))
        if self.csv is not None:
            csviter = util.IterWrap(iter(self.csv.rows))
        else:
            csviter = None
        for entry in self.table:
            # Try and find corresponding mapping object
            mapobj = mapiter.until(lambda x: x.vrom_start >= entry.virt_start)
            # Find corresponding CSV row if provided
            if csviter is not None:
                row = csviter.until(lambda x: x.start >= entry.virt_start)
            else:
                row = None
            # Create and insert VFile
            if mapobj is not None and entry.virt_start == mapobj.vrom_start:
                vfile = VFile(table_entry=entry, mapping_obj=mapobj, csv_row=row)
            else:
                vfile = VFile(table_entry=entry, csv_row=row)
            vfiles[entry.virt_start] = vfile
        return SortedDict(vfiles)

    @property
    def csv(self):
        """ Get the `CsvInfo` object with row data. """
        return self._csv

    @property
    def mapping(self):
        """ Get the mapping object. """
        return self._mapping

    @property
    def table(self):
        """ Get the `dmadata.Table`. """
        return self._table

    @property
    def vfiles(self):
        """ Get the `VFile` mapping. """
        return self._vfiles

    def resolve(self, vrom):
        """ Resolve a `VFile` given a virtual ROM address. """
        # Use bisect of SortedDict keys for efficient retrieval of VFile from virtual ROM address.
        index = self.vfiles.bisect_right(vrom) - 1
        if index in range(len(self.vfiles)):
            return self.vfiles.items()[index][1]
