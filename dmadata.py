import ctypes

DMADATA_MM = 0x1A500

class RomFile(object):
    """ ROM file. """
    def __init__(self, fp, offset):
        self._fp = fp
        self._offset = offset
        self._table = Table.read(self.fp, self.offset)

    @property
    def fp(self):
        """ Get the file pointer. """
        return self._fp

    @property
    def offset(self):
        """ Get the offset to the dmadata table. """
        return self._offset

    @property
    def table(self):
        """ Get the dmadata `Table` object. """
        return self._table

    def read(self, vrom, length):
        """ Read data from the underlying file using a VROM address. """
        offset = self.table.resolve(vrom)
        return self.read_phys(offset, length)

    def read_phys(self, offset, length):
        """ Read data from the underlying file using a PROM (physical) address. """
        self.fp.seek(offset)
        return self.fp.read(length)

class TableEntry(ctypes.BigEndianStructure):
    """ Table entry structure. """
    _fields_ = (
        ('virt_start', ctypes.c_uint),
        ('virt_end', ctypes.c_uint),
        ('phys_start', ctypes.c_uint),
        ('phys_end', ctypes.c_uint),
    )

    def __init__(self, index, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._index = index

    @property
    def index(self):
        """ Get the entry index. """
        return self._index

    @property
    def prom(self):
        """ Get physical ROM addresses as a `range`. """
        if self.is_decompressed():
            return range(self.phys_start, self.phys_start + self.decompressed_size())
        elif not self.does_not_exist():
            return range(self.phys_start, self.phys_end)

    @property
    def vrom(self):
        """ Get virtual ROM addresses as a `range`. """
        return range(self.virt_start, self.virt_end)

    def empty(self):
        """ Whether or not this entry is "empty." """
        return self.virt_start == self.virt_end == self.phys_start == self.phys_end == 0

    def does_not_exist(self):
        """ Whether or not this is a "DoesNotExist" file entry. """
        return self.phys_end == 0xFFFFFFFF

    def is_decompressed(self):
        """ Whether or not this data is decompressed. """
        return self.phys_end == 0

    def decompressed_size(self):
        """ Get size of decompressed data. """
        return self.virt_end - self.virt_start

    def read(self, file):
        """ Read a `TableEntry` from a file. """
        self.receiveSome(file.read(ctypes.sizeof(self)))
        return self

    def receiveSome(self, data):
        """ Parse from bytes. """
        fit = min(len(data), ctypes.sizeof(self))
        ctypes.memmove(ctypes.addressof(self), data, fit)

class Table(object):
    """ Parsed file table with entries. """
    def __init__(self, offset, entries):
        self._offset = offset
        self._entries = entries

    def __iter__(self):
        return iter(self.entries)

    @property
    def entries(self):
        """ Get the `TableEntry` entries. """
        return self._entries

    @staticmethod
    def read(file, offset):
        """ Read a `Table` from a file and offset. """
        entries = tuple(read_table(file, offset))
        return Table(offset, entries)

    @staticmethod
    def read_path(path, offset):
        """ Read a `Table` from a file and offset, given a file path. """
        with open(path, 'rb') as f:
            return Table.read(f, offset)

    def get_index(self, virt_start):
        """ Get the index of an entry given a virtual start address. """
        for i, entry in enumerate(self._entries):
            if entry.virt_start == virt_start:
                return i, entry

    def resolve(self, address, virt=False):
        """ Resolve a virtual ROM address to a physical ROM address, or the other way around. """
        def resolve_from_range(address, addr_range, base):
            if address in addr_range:
                offset = address - addr_range.start
                return base + offset
        for entry in self._entries:
            if entry.empty() or entry.does_not_exist():
                continue
            if entry.is_decompressed():
                if virt:
                    # If virt=True, resolve: physical => virtual
                    result = resolve_from_range(address, entry.prom, entry.vrom.start)
                else:
                    # If virt=False, resolve: virtual => physical
                    result = resolve_from_range(address, entry.vrom, entry.prom.start)
                if result is not None:
                    return result
            else:
                raise Exception('Must be a fully decompressed ROM! {:08X}'.format(entry.virt_start))

def read_table(file, offset):
    """ Generator for reading all table entries in a file. """
    index = 0
    table_start = offset
    table_entry = None
    while True:
        file.seek(offset)
        entry = TableEntry(index).read(file)

        # Check if found table entry
        if entry.phys_start == table_start:
            table_entry = entry

        # Check if at end of table
        if table_entry is not None:
            end = table_entry.phys_start + table_entry.decompressed_size()
            if offset >= end:
                break

        index += 1
        offset += ctypes.sizeof(entry)
        yield entry
