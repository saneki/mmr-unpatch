import ctypes

def deserialize(kls, data):
    """ Deserialize a ctypes instance. """
    inst = kls()
    fit = min(len(data), ctypes.sizeof(inst))
    ctypes.memmove(ctypes.addressof(inst), data, fit)
    success = fit == ctypes.sizeof(inst)
    return (inst, success)

def takeuntil(predicate, iterator):
    """ Take items from an iterator until the given predicate returns True, and return that item. """
    while True:
        x = next(iterator)
        if predicate(x):
            return x

class IterWrap(object):
    """ Iterator wrapper class. """
    def __init__(self, child):
        self._child = child
        self._state = (None, False, False)

    @property
    def child(self):
        """ Get child iterator. """
        return self._child

    @property
    def state(self):
        """ Get current state tuple: (value, started, ended). """
        return self._state

    def next(self):
        """ Advance iterator and current state tuple. """
        try:
            value = next(self.child)
            self._state = (value, True, False)
        except StopIteration:
            self._state = (None, True, True)
        return self.state

    def until(self, check):
        """ Advance until check returns true. """
        value, started, ended = self.state
        if not started:
            value, started, ended = self.next()
        while not ended and not check(value):
            value, started, ended = self.next()
        return value
