import csv

class CsvRow(object):
    """ CSV row containing info about virtual files. """
    def __init__(self, row):
        self._row = row

    @property
    def description(self):
        """ Get the virtual file description (may be empty). """
        return self.row[11]

    @property
    def end(self):
        """ Get the virtual file ROM end address. """
        return int(self.row[2])

    @property
    def has_description(self):
        """ Get whether or not this row contains a valid description. """
        return self.description != '' and self.description != '(?)'

    @property
    def has_name(self):
        """ Get whether or not this row contains a valid name. """
        return self.name != '' and self.name != '(?)'

    @property
    def index(self):
        """ Get the virtual file index. """
        return int(self.row[0])

    @property
    def name(self):
        """ Get the virtual file name. """
        return self.row[10]

    @property
    def row(self):
        """ Get the underlying row. """
        return self._row

    @property
    def start(self):
        """ Get the virtual file ROM start address. """
        return int(self.row[1])

class CsvInfo(object):
    """ Handles parsing `VRomFile` info from a specific CSV file. """
    def __init__(self, rows=tuple()):
        self._rows = rows

    @staticmethod
    def from_file(csvfile):
        """ Construct by parsing `VRomFile` entries from a CSV file. """
        rows = tuple(CsvInfo.generate_files(csvfile))
        return CsvInfo(rows)

    @staticmethod
    def from_path(filepath):
        """ Construct by parsing `VRomFile` entries from a CSV file at the given path. """
        with open(filepath, 'r') as csvfile:
            return CsvInfo.from_file(csvfile)

    @staticmethod
    def generate_files(csvfile):
        """
        Parse CSV file with `CsvRow` list information.

        Reference: "The Ultimate MM Spreadsheet - File List (US) 1.0.csv"
        """
        reader = csv.reader(csvfile)
        for row in reader:
            # Ignore first row of headers
            if row[0] != '#':
                yield CsvRow(row)

    @property
    def rows(self):
        """ Get the `CsvRow` entries. """
        return self._rows

    def find(self, address):
        """ Find the `CsvRow` which contains the given address. """
        for row in self.rows:
            if row.start <= address < row.end:
                return row
